﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using PhoneSender;

namespace TestDictionary
{
    [TestFixture]
    public class NUnitTestTranslator
    {
        [Test]
        public void TestTranslatorOutputIsString()
        {
            //Arrange 
            Translator t = new Translator();

            //Act
            var res = t.Translate("a");

            //Assert
            Assert.True(res is String);
        }

        [Test]
        [TestCase ("hh")]
        public void TestTranslatorInputDouble(string input)
        {
            //Arrange 
            Translator t = new Translator();

            //Act
            var res = t.Translate(input);

            //Assert
            Assert.AreEqual("444 444", res);
        }
    }      
}
