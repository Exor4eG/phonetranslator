﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneSender
{
    public class Translator
    {
        public string Translate(string input)
        {
            string str = null;
            char lastSymb = ' ';
            foreach (char c in input)
            {
                string add = PhoneDictionary.dict[c].ToString();
                if (add[0] == lastSymb)
                    str = (str + " " + add);
                else
                    str += add;
                lastSymb = add[add.Length - 1];
            }
            return str;
        }
    }
}
